import styled from 'styled-components';

export const Wrapper = styled.div`
  margin: 20px;
`;

export const Container = styled.div`
  display: grid;
  grid-gap: 6px;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  grid-template-rows: 1fr 1fr 1fr 1fr 1fr 1fr;
  box-sizing: border-box;
  width: 100%;
  max-width: 500px;
  margin: 0 auto;
  background-color: #888;
  padding: 20px;
  border-radius: 6px;
`;

export const Button = styled.button`
  padding: 20px;
  border-radius: 6px;
  min-width: 100%;
  word-break: break-word;

  :focus {
    background: red;
    outline: 1px solid #555;
    outline-offset: 2px;
  }
  @media (max-width: 330px) {
    padding: 8px;
  }
`;

export const Display = styled.div`
  background: #fff;
  padding: 20px;
  grid-column-start: 1;
  grid-column-end: 5;
  text-align: right;
  margin-bottom: 10px;
`;
