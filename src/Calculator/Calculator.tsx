import React, { useState, useEffect, useCallback } from 'react';
import * as S from './Calculator.style';
import { parseEquation, Operators } from '../shared/utils';

const INPUT_CHARACTER_LIMIT = 16;

// type TNumbers = '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | '.';

export default function Calculator() {
  const [calculationArr, setCalculationArr] = useState<(number | Operators)[]>(
    [],
  );

  const [isOperatorLastAction, setOperatorLastAction] = useState(false);

  const [currentValue, setCurrentValue]: [
    number | string,
    (currentValue: number | string) => void,
  ] = useState<number | string>(0);

  const handleInput = (event: React.MouseEvent<HTMLButtonElement>) => {
    let value = event.currentTarget.value;

    if (currentValue.toString().length > INPUT_CHARACTER_LIMIT) {
      return;
    }

    let currentStringValue = '';
    if (isOperatorLastAction) {
      currentStringValue = value;
      setOperatorLastAction(false);
    } else {
      currentStringValue = currentValue.toString() + value;
    }

    if (value === '.') {
      if (currentValue.toString().indexOf('.') === -1) {
        setCurrentValue(+currentStringValue);
      }
    } else {
      setCurrentValue(+currentStringValue);
    }
  };

  const handleOperator = (event: React.MouseEvent<HTMLButtonElement>) => {
    let operator: Operators;
    let symbol: string;

    const textOperator = event.currentTarget.value;
    switch (textOperator) {
      case '+':
        operator = Operators.ADD;
        symbol = '+';
        break;
      case '-':
        operator = Operators.SUBTRACT;
        symbol = '-';
        break;
      case '*':
        operator = Operators.MULTIPLY;
        symbol = '×';
        break;
      case '/':
        operator = Operators.DIVIDE;
        symbol = '÷';
        break;
      default:
        throw new Error('Incorrect operator type supplied');
    }
    setCalculationArr((prevState) => {
      const newArr = [...prevState];
      newArr.push(+currentValue);
      newArr.push(operator);
      return newArr;
    });
    setOperatorLastAction(true);
    setCurrentValue(symbol);
  };

  const handleClear = () => {
    //
  };

  const handleCalculate = () => {
    //
    setCalculationArr((prevState) => {
      const newArr = [...prevState];
      newArr.push(+currentValue);
      return newArr;
    });
    const answer = parseEquation([...calculationArr, +currentValue]);
    setCurrentValue(answer);
  };

  console.log(currentValue);

  return (
    <S.Wrapper>
      <S.Container>
        <S.Display data-testid="calculator-display">{currentValue}</S.Display>
        <S.Button
          onClick={handleOperator}
          value="abs"
          data-testid="calculator-operator-abs"
        >
          +/-
        </S.Button>
        <S.Button data-testid="calculator-operator-percentage">%</S.Button>
        <S.Button data-testid="calculator-operator-sqrt">√</S.Button>
        <S.Button onClick={handleClear} data-testid="calculator-key-clear">
          C CE
        </S.Button>
        <S.Button
          onClick={handleInput}
          value="7"
          data-testid="calculator-key-7"
        >
          7
        </S.Button>
        <S.Button
          onClick={handleInput}
          value="8"
          data-testid="calculator-key-8"
        >
          8
        </S.Button>
        <S.Button
          onClick={handleInput}
          value="9"
          data-testid="calculator-key-9"
        >
          9
        </S.Button>
        <S.Button
          onClick={handleOperator}
          value="/"
          data-testid="calculator-operator-divide"
        >
          ÷
        </S.Button>
        <S.Button
          onClick={handleInput}
          value="4"
          data-testid="calculator-key-4"
        >
          4
        </S.Button>
        <S.Button
          onClick={handleInput}
          value="5"
          data-testid="calculator-key-5"
        >
          5
        </S.Button>
        <S.Button
          onClick={handleInput}
          value="6"
          data-testid="calculator-key-6"
        >
          6
        </S.Button>
        <S.Button
          onClick={handleOperator}
          value="*"
          data-testid="calculator-operator-multiply"
        >
          ×
        </S.Button>
        <S.Button
          onClick={handleInput}
          value="1"
          data-testid="calculator-key-1"
        >
          1
        </S.Button>
        <S.Button
          onClick={handleInput}
          value="2"
          data-testid="calculator-key-2"
        >
          2
        </S.Button>
        <S.Button
          onClick={handleInput}
          value="3"
          data-testid="calculator-key-3"
        >
          3
        </S.Button>
        <S.Button
          onClick={handleOperator}
          value="-"
          data-testid="calculator-operator-minus"
        >
          -
        </S.Button>
        <S.Button
          onClick={handleInput}
          value="0"
          data-testid="calculator-key-0"
        >
          0
        </S.Button>
        <S.Button
          onClick={handleInput}
          value="."
          data-testid="calculator-key-dot"
        >
          .
        </S.Button>
        <S.Button
          data-testid="calculator-key-equals"
          value="="
          onClick={handleCalculate}
        >
          =
        </S.Button>
        <S.Button
          onClick={handleOperator}
          value="+"
          data-testid="calculator-operator-plus"
        >
          +
        </S.Button>
      </S.Container>
      <div>currentValue: {currentValue}</div>
      <div>calc arr: {calculationArr}</div>
    </S.Wrapper>
  );
}
